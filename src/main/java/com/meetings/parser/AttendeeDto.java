package com.meetings.parser;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Map;

/**
 * Simple Attendee Data Transfer Object.
 */
public class AttendeeDto {

    public String id;

    public ZoneOffset timezone;

    public LocalTime[] workingHours;

    public Map<LocalDate, LocalTime[][]> booked;

    public LocalTime[][] dailyBooked;

    public Map<DayOfWeek, LocalTime[][]> weeklyBooked;

}
