package com.meetings.parser.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.ZoneOffset;

/**
 * Deserializes ZoneOffset JSON property value.
 */
public class ZoneOffsetDeserializer extends JsonDeserializer<ZoneOffset> {

    @Override
    public ZoneOffset deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        String value = parser.getText();
        return ZoneOffset.of(value);
    }

}
