package com.meetings.parser.deserializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

import java.io.IOException;
import java.time.DayOfWeek;

/**
 * Deserializes DayOfWeek JSON key.
 */
public class DayOfWeekKeyDeserializer extends KeyDeserializer {

    @Override
    public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return DayOfWeek.valueOf(key.toUpperCase());
    }

}
