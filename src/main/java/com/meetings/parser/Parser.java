package com.meetings.parser;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.meetings.parser.deserializer.DayOfWeekKeyDeserializer;
import com.meetings.parser.deserializer.LocalDateKeyDeserializer;
import com.meetings.parser.deserializer.LocalTimeDeserializer;
import com.meetings.parser.deserializer.ZoneOffsetDeserializer;

import java.io.IOException;
import java.nio.file.Path;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;

/**
 * Input file parser.
 */
public class Parser {

    private ObjectMapper mapper;

    public Parser() {
        SimpleModule module =
                new SimpleModule("ProjectSerializationModule",
                        new Version(1, 0, 0, null, null, null));
        module.addDeserializer(LocalTime.class, new LocalTimeDeserializer());
        module.addDeserializer(ZoneOffset.class, new ZoneOffsetDeserializer());
        module.addKeyDeserializer(LocalDate.class, new LocalDateKeyDeserializer());
        module.addKeyDeserializer(DayOfWeek.class, new DayOfWeekKeyDeserializer());
        mapper = new ObjectMapper();
        mapper.registerModule(module);
    }

    /**
     * Parses input file.
     *
     * @param filePath
     * @return
     * @throws IOException
     */
    public List<AttendeeDto> parse(Path filePath) throws IOException {
        AttendeeDto[] attendees = mapper.readValue(filePath.toFile(), AttendeeDto[].class);
        return Arrays.asList(attendees);
    }

}
