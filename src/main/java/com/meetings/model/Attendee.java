package com.meetings.model;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.OffsetDateTimeSlot;
import com.meetings.model.booking.BookingFilter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents an attendee with booked time slots transformed into BookingFilters.
 */
public class Attendee {

    private String id;

    private ZoneOffset timezone;

    private List<BookingFilter> bookings = new ArrayList<>();

    public Attendee(String id, ZoneOffset timezone, List<BookingFilter> bookings) {
        this.id = id;
        this.timezone = timezone;
        this.bookings = bookings;
    }

    public String getId() {
        return id;
    }

    public ZoneOffset getTimezone() {
        return timezone;
    }

    /**
     * Finds available time slots within a given time frame.
     * @param offsetTimeFrame
     * @return list off time slots (same offset as in offsetTimeFrame parameter)
     */
    public List<OffsetDateTimeSlot> findTimeSlots(OffsetDateTimeSlot offsetTimeFrame) {
        OffsetDateTimeSlot attendeeOffsetTimeFrame = offsetTimeFrame.withOffsetSameInstant(timezone);
        LocalDateTimeSlot timeFrame = attendeeOffsetTimeFrame.toLocalDateTimeSlot();
        List<LocalDateTimeSlot> timeSlots = findTimeSlots(timeFrame);
        return timeSlots.stream()
                .map(timeSlot -> OffsetDateTimeSlot.of(timeSlot, timezone).withOffsetSameInstant(offsetTimeFrame.getOffset()))
                .collect(Collectors.toList());
    }

    /**
     * Finds available time slots within a given time frame.
     * @param timeFrame
     * @return
     */
    protected List<LocalDateTimeSlot> findTimeSlots(LocalDateTimeSlot timeFrame) {
        List<LocalDateTimeSlot> slots = new ArrayList<>();
        LocalDateTime startFrom = timeFrame.getBeginning();
        LocalDateTime timeFrameEnd = timeFrame.getEnd();
        while (startFrom.isBefore(timeFrameEnd) &&
                !startFrom.equals(timeFrameEnd)) {
            LocalDateTimeSlot timeSlot = findNearestTimeSlot(new LocalDateTimeSlot(startFrom, timeFrameEnd));
            startFrom = timeSlot.getEnd();
            if (timeSlot.getDuration().isZero()) {
                startFrom = startFrom.plusNanos(1);
            } else {
                slots.add(timeSlot);
            }
        }
        return slots;
    }

    /**
     * Finds an empty time slot within a given time frame.
     * Resulted time slot is as close to time frame beginning as possible.
     * If time slot is impossible to find an empty time slot is returned.
     * @param timeFrame
     * @return
     */
    protected LocalDateTimeSlot findNearestTimeSlot(LocalDateTimeSlot timeFrame) {
        LocalDateTimeSlot timeSlot = timeFrame;
        for (BookingFilter booking : bookings) {
            timeSlot = booking.ceilingTimeSlot(timeSlot);
        }
        return timeSlot;
    }

}
