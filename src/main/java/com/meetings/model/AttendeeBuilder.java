package com.meetings.model;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.LocalTimeSlot;
import com.meetings.model.booking.*;
import com.meetings.parser.AttendeeDto;

import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Build Attendee out from AttendeeDto
 */
public class AttendeeBuilder {

    private Set<String> uniqueIds = new HashSet<>();

    /**
     * Builds a list of Attendees
     *
     * @param dtos
     * @return
     */
    public List<Attendee> build(List<AttendeeDto> dtos) {
        return dtos.stream().map(this::build).collect(Collectors.toList());
    }

    /**
     * Builds an Attendee
     *
     * @param dto
     * @return
     */
    public Attendee build(AttendeeDto dto) {
        validate(dto);
        String id = dto.id;
        ZoneOffset zoneOffset = dto.timezone;
        List<BookingFilter> bookings = new ArrayList<>();
        bookings.add(new WeekendBookingFilter());
        bookings.add(new WorkingHoursBookingFilter(buildTimeSlot(dto.workingHours)));
        bookings.add(new DailyBookingFilter(buildTimeSlots(dto.dailyBooked)));
        bookings.add(new WeeklyBookingFilter(buildWeeklyTimeSlots(dto.weeklyBooked)));
        bookings.add(new SpecificBookingFilter(buildDateTimeSlots(dto.booked)));
        return new Attendee(id, zoneOffset, bookings);
    }

    /**
     * Resets unique ids store.
     */
    public void resetUniqueIds() {
        uniqueIds.clear();
    }

    private void validate(AttendeeDto attendeeDto) {
        if (uniqueIds.contains(attendeeDto.id)) {
            throw new IllegalArgumentException("Duplicated Attendee Id: " + attendeeDto.id);
        }
        uniqueIds.add(attendeeDto.id);
        if (attendeeDto.timezone == null) {
            throw new IllegalArgumentException("Attendee without a timezone: " + attendeeDto.id);
        }
        if (attendeeDto.workingHours == null) {
            throw new IllegalArgumentException("Attendee without a workingHours: " + attendeeDto.id);
        }
    }

    private Map<DayOfWeek, List<LocalTimeSlot>> buildWeeklyTimeSlots(Map<DayOfWeek, LocalTime[][]> localDateTimes) {
        if (localDateTimes == null) {
            return Collections.emptyMap();
        }
        Map<DayOfWeek, List<LocalTimeSlot>> weeklyBookings = new HashMap<>();
        localDateTimes.entrySet().stream()
                .forEach(entry -> {
                    List<LocalTimeSlot> slots = buildTimeSlots(entry.getValue());
                    weeklyBookings.put(entry.getKey(), slots);
                });
        return weeklyBookings;
    }

    private List<LocalDateTimeSlot> buildDateTimeSlots(Map<LocalDate, LocalTime[][]> localDateTimes) {
        if (localDateTimes == null) {
            return Collections.emptyList();
        }
        return localDateTimes.entrySet().stream()
                .map(entry -> this.buildDateTimeSlots(entry.getKey(), entry.getValue()))
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<LocalDateTimeSlot> buildDateTimeSlots(LocalDate localDate, LocalTime[][] localTimeSlots) {
        return Arrays.asList(localTimeSlots).stream()
                .map(localTimeSlot -> this.buildDateTimeSlot(localDate, localTimeSlot))
                .collect(Collectors.toList());
    }

    private LocalDateTimeSlot buildDateTimeSlot(LocalDate localDate, LocalTime[] localTimeSlot) {
        if (localDate == null || localTimeSlot == null || localTimeSlot.length != 2) {
            throw new IllegalArgumentException("Could not create DateTimeSlot beginning: " + localDate + " " + Arrays.toString(localTimeSlot));
        }
        LocalDateTime from = LocalDateTime.of(localDate, localTimeSlot[0]);
        LocalDateTime to = LocalDateTime.of(localDate, localTimeSlot[1]);
        return new LocalDateTimeSlot(from, to);
    }

    private List<LocalTimeSlot> buildTimeSlots(LocalTime[][] localTimeSlots) {
        if (localTimeSlots == null) {
            return new ArrayList<>();
        }
        return Arrays.asList(localTimeSlots).stream()
                .map(localTimeSlot -> buildTimeSlot(localTimeSlot))
                .collect(Collectors.toList());
    }

    private LocalTimeSlot buildTimeSlot(LocalTime[] localTimeSlot) {
        if (localTimeSlot == null || localTimeSlot.length != 2) {
            throw new IllegalArgumentException("Could not create TimeSlot beginning: " + Arrays.toString(localTimeSlot));
        }
        LocalTime from = localTimeSlot[0];
        LocalTime to = localTimeSlot[1];
        return new LocalTimeSlot(from, to);
    }

}
