package com.meetings.model;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.OffsetDateTimeSlot;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Finds best suited meeting time slots across multiple attendees.
 */
public class MeetingPlanner {

    private ZoneOffset resultOffset;

    public MeetingPlanner(ZoneOffset offset) {
        this.resultOffset = offset;
    }

    /**
     * Finds best meeting slots with given duration or longer.
     * If solution for all attendees is not found returns partial solution.
     * @param attendees
     * @param timeFrame
     * @param duration
     */
    public List<MeetingSlot> findBestMeetingSlots(List<Attendee> attendees, LocalDateTimeSlot timeFrame, Duration duration) {
        List<MeetingSlot> meetingSlots = findMeetingSlots(attendees, timeFrame, duration);
        if (meetingSlots.size() > 0 && meetingSlots.get(0).getExcludedAttendees().size() == 0) {
            meetingSlots = meetingSlots.stream()
                    .filter(meetingSlot -> meetingSlot.getExcludedAttendees().size() == 0)
                    .collect(Collectors.toList());
        }
        return meetingSlots;
    }

    /**
     * Finds all meeting slots with given duration or longer.
     * Result may contain meeting slots that excludes some attendees.
     * @param attendees
     * @param timeFrame
     * @param duration
     * @return
     */
    private List<MeetingSlot> findMeetingSlots(List<Attendee> attendees, LocalDateTimeSlot timeFrame, Duration duration) {
        OffsetDateTimeSlot offsetTimeFrame = OffsetDateTimeSlot.of(timeFrame, resultOffset);
        List<AttendeeNode> nodes = attendees.stream()
                .map(a -> buildAttendeeNode(a, offsetTimeFrame))
                .collect(Collectors.toList());
        SortedSet<MeetingSlot> result = new TreeSet<>(new MeetingSlotComparator());
        for (AttendeeNode node : nodes) {
            for (LocalDateTimeSlot slot : node.getTimeSlots()) {
                MeetingSlot meetingSlot = buildMeetingSlot(nodes, slot);
                if (meetingSlot.getIncludedAttendees().size() > 0 &&
                        meetingSlot.getTimeSlot().getDuration().compareTo(duration) >= 0) {
                    result.add(buildMeetingSlot(nodes, slot));
                }
            }
        }
        return new ArrayList<>(result);
    }

    private AttendeeNode buildAttendeeNode(Attendee attendee, OffsetDateTimeSlot timeFrame) {
        List<OffsetDateTimeSlot> offsetSlots = attendee.findTimeSlots(timeFrame);
        List<LocalDateTimeSlot> localSlots = offsetSlots.stream()
                .map(offsetSlot -> offsetSlot.withOffsetSameInstant(resultOffset).toLocalDateTimeSlot())
                .collect(Collectors.toList());
        return new AttendeeNode(attendee, localSlots);
    }

    private MeetingSlot buildMeetingSlot(List<AttendeeNode> nodes, LocalDateTimeSlot slot) {
        LocalDateTime startingPoint = slot.getBeginning();
        LocalDateTime finishPoint = slot.getEnd();
        List<Attendee> includedAttendees = new ArrayList<>();
        List<Attendee> excludedAttendees = new ArrayList<>();
        for (AttendeeNode node : nodes) {
            LocalDateTimeSlot subTimeSlot = node.findTimeSlot(startingPoint);
            if (subTimeSlot != null && subTimeSlot.contains(startingPoint)) {
                includedAttendees.add(node.getAttendee());
                finishPoint = min(finishPoint, subTimeSlot.getEnd());
            } else {
                excludedAttendees.add(node.getAttendee());
            }
        }
        LocalDateTimeSlot meetingTimeSlot = new LocalDateTimeSlot(startingPoint, finishPoint);
        return new MeetingSlot(includedAttendees, excludedAttendees, meetingTimeSlot);
    }

    private LocalDateTime min(LocalDateTime a, LocalDateTime b) {
        return a.isBefore(b) ? a : b;
    }

}
