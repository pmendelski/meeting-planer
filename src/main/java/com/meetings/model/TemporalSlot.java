package com.meetings.model;

import java.time.*;
import java.time.temporal.Temporal;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Represents a time interval with a beginning and an end.
 * @param <T> interval time unit
 * @param <S> child class
 */
@SuppressWarnings("unchecked")
public abstract class TemporalSlot<T extends Temporal & Comparable, S extends TemporalSlot<T, S>> {

    protected final T beginning;
    protected final T end;
    private final TemporalSlotBuilder<S, T> builder;

    public TemporalSlot(T beginning, T end, TemporalSlotBuilder<S, T> builder) {
        this.beginning = beginning;
        this.end = end;
        this.builder = builder;
        if (beginning.compareTo(end) > 0) {
            throw new IllegalArgumentException("TemporalSlot cannot have negative duration");
        }
    }

    public static <T extends Temporal & Comparable, S extends TemporalSlot<T, S>> TreeMap<T, S> mapByBeginning(List<S> slots) {
        TreeMap<T, S> result = new TreeMap<>();
        for (S timeSlot : slots) {
            T from = timeSlot.getBeginning();
            Map.Entry<T, S> previousTimeSlotEntry = result.floorEntry(from);
            if (previousTimeSlotEntry != null) {
                S previousTimeSlot = previousTimeSlotEntry.getValue();
                result.put(from, timeSlot.maximizeTo(previousTimeSlot));
            } else {
                result.put(from, timeSlot);
            }
        }
        return result;
    }

    public T getBeginning() {
        return beginning;
    }

    public T getEnd() {
        return end;
    }

    public Duration getDuration() {
        return Duration.between(beginning, end);
    }

    public boolean contains(T time) {
        return !isAfter(time) && !isBefore(time);
    }

    public S maximizeTo(S temporalSlot) {
        T maxTo = this.isBefore(temporalSlot.getEnd()) ? temporalSlot.getEnd() : this.end;
        return builder.build(this.beginning, maxTo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TemporalSlot)) return false;
        TemporalSlot<?, ?> that = (TemporalSlot<?, ?>) o;
        if (!beginning.equals(that.beginning)) return false;
        return end.equals(that.end);
    }

    @Override
    public int hashCode() {
        int result = beginning.hashCode();
        result = 31 * result + end.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" +
                "beginning=" + beginning +
                ", end=" + end +
                '}';
    }

    public boolean isBefore(T time) {
        int cmp = end.compareTo(time);
        return cmp <= 0;
    }

    public boolean isAfter(T time) {
        int cmp = beginning.compareTo(time);
        return cmp > 0;
    }

    @FunctionalInterface
    protected interface TemporalSlotBuilder<S, T> {
        S build(T from, T to);
    }

    public static class OffsetDateTimeSlot extends TemporalSlot<OffsetDateTime, OffsetDateTimeSlot> {

        public OffsetDateTimeSlot(OffsetDateTime from, OffsetDateTime to) {
            super(from, to, OffsetDateTimeSlot::new);
            if (!from.getOffset().equals(to.getOffset())) {
                throw new IllegalArgumentException("OffsetDateTimeSlot must be declared on the same ZoneOffset");
            }
        }

        public static OffsetDateTimeSlot of(LocalDateTimeSlot slot, ZoneOffset offset) {
            OffsetDateTime from = OffsetDateTime.of(slot.beginning, offset);
            OffsetDateTime to = OffsetDateTime.of(slot.end, offset);
            return new OffsetDateTimeSlot(from, to);
        }

        public OffsetDateTimeSlot withOffsetSameInstant(ZoneOffset offset) {
            OffsetDateTime from = this.getBeginning().withOffsetSameInstant(offset);
            OffsetDateTime to = this.getEnd().withOffsetSameInstant(offset);
            return new OffsetDateTimeSlot(from, to);
        }

        public LocalDateTimeSlot toLocalDateTimeSlot() {
            return new LocalDateTimeSlot(beginning.toLocalDateTime(), end.toLocalDateTime());
        }

        public ZoneOffset getOffset() {
            return beginning.getOffset();
        }

    }

    public static class LocalDateTimeSlot extends TemporalSlot<LocalDateTime, LocalDateTimeSlot> {

        public LocalDateTimeSlot(LocalDateTime from, LocalDateTime to) {
            super(from, to, LocalDateTimeSlot::new);
        }

    }

    public static class LocalTimeSlot extends TemporalSlot<LocalTime, LocalTimeSlot> {

        public LocalTimeSlot(LocalTime from, LocalTime to) {
            super(from, to, LocalTimeSlot::new);
        }

    }

}
