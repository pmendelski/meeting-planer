package com.meetings.model;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;

import java.util.Collections;
import java.util.List;

/**
 * Represents a meeting slot.
 */
public class MeetingSlot {
    private List<Attendee> included;
    private List<Attendee> excluded;
    private LocalDateTimeSlot localDateTimeSlot;

    public MeetingSlot(List<Attendee> included, List<Attendee> excluded, LocalDateTimeSlot localDateTimeSlot) {
        this.included = included;
        this.excluded = excluded;
        this.localDateTimeSlot = localDateTimeSlot;
    }

    public List<Attendee> getIncludedAttendees() {
        return Collections.unmodifiableList(included);
    }

    public List<Attendee> getExcludedAttendees() {
        return Collections.unmodifiableList(excluded);
    }

    public LocalDateTimeSlot getTimeSlot() {
        return localDateTimeSlot;
    }
}
