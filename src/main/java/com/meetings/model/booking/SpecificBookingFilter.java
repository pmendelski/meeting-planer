package com.meetings.model.booking;

import com.meetings.model.TemporalSlot;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Filters specific date booked time slots.
 */
public class SpecificBookingFilter extends BookingFilter {

    private TreeMap<LocalDateTime, LocalDateTimeSlot> booked;

    public SpecificBookingFilter(List<LocalDateTimeSlot> bookings) {
        this.booked = TemporalSlot.mapByBeginning(bookings);
    }

    protected LocalDateTime findTimeSlotBeginning(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalDateTime result = beginning;
        Map.Entry<LocalDateTime, LocalDateTimeSlot> bookedBeforeEntry = booked.floorEntry(beginning);
        LocalDateTimeSlot bookedTimeSlot = bookedBeforeEntry != null ? bookedBeforeEntry.getValue() : null;
        if (bookedTimeSlot != null && bookedTimeSlot.contains(beginning)) {
            result = bookedTimeSlot.getEnd();
        }
        return normalize(result, timeFrame);
    }

    protected LocalDateTime findTimeSlotEnd(LocalDateTimeSlot timeFrame) {
        LocalDateTime result = timeFrame.getEnd();
        LocalDateTime beginning = timeFrame.getBeginning();
        Map.Entry<LocalDateTime, LocalDateTimeSlot> nextBookedEntry = booked.ceilingEntry(beginning);
        result = nextBookedEntry == null ? result : nextBookedEntry.getValue().getBeginning();
        return normalize(result, timeFrame);
    }
}
