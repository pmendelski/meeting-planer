package com.meetings.model.booking;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;

import java.time.LocalDateTime;

/**
 * Base for all meeting time filters
 */
public abstract class BookingFilter {

    /**
     * Finds not booked time slot within a given time frame.
     * Resulted time slot is as close to time frame beginning as possible.
     * If time slot is impossible to find an empty time slot is returned.
     * @param timeFrame
     * @return
     */
    public LocalDateTimeSlot ceilingTimeSlot(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = findTimeSlotBeginning(timeFrame);
        LocalDateTime end = findTimeSlotEnd(new LocalDateTimeSlot(beginning, timeFrame.getEnd()));
        return new LocalDateTimeSlot(beginning, end);
    }

    /**
     * Finds the nearest time slot beginning.
     * @param timeFrame
     * @return
     */
    protected abstract LocalDateTime findTimeSlotBeginning(LocalDateTimeSlot timeFrame);

    /**
     * Finds the furthest time slot end.
     * @param timeFrame
     * @return
     */
    protected abstract LocalDateTime findTimeSlotEnd(LocalDateTimeSlot timeFrame);

    /**
     * Makes sure that result is in given time frame.
     * @param dataTime
     * @param timeFrame
     * @return
     */
    protected LocalDateTime normalize(LocalDateTime dataTime, LocalDateTimeSlot timeFrame) {
        if (timeFrame.isBefore(dataTime)) {
            return timeFrame.getEnd();
        }
        if (timeFrame.isAfter(dataTime)) {
            return timeFrame.getBeginning();
        }
        return dataTime;
    }

}
