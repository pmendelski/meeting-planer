package com.meetings.model.booking;

import com.meetings.model.TemporalSlot;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.LocalTimeSlot;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/**
 * Filters weekly booked time slots.
 */
public class WeeklyBookingFilter extends BookingFilter {

    private Map<DayOfWeek, TreeMap<LocalTime, LocalTimeSlot>> weeklyBooked;

    public WeeklyBookingFilter(Map<DayOfWeek, List<LocalTimeSlot>> weeklyBooked) {
        this.weeklyBooked = new EnumMap<>(DayOfWeek.class);
        weeklyBooked.entrySet().stream()
                .forEach(entry -> this.weeklyBooked.put(entry.getKey(), TemporalSlot.mapByBeginning(entry.getValue())));
        Arrays.asList(DayOfWeek.values()).stream()
                .filter(d -> !this.weeklyBooked.containsKey(d))
                .forEach(d -> this.weeklyBooked.put(d, new TreeMap<>()));
    }

    @Override
    protected LocalDateTime findTimeSlotBeginning(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalDateTime result = beginning;
        LocalTime localTime = beginning.toLocalTime();
        LocalDate localDate = beginning.toLocalDate();
        Map.Entry<LocalTime, LocalTimeSlot> bookedWeeklyBeforeEntry = weeklyBooked.get(localDate.getDayOfWeek()).floorEntry(localTime);
        LocalTimeSlot weeklyBookedTimeSlot = bookedWeeklyBeforeEntry != null ? bookedWeeklyBeforeEntry.getValue() : null;
        if (weeklyBookedTimeSlot != null && weeklyBookedTimeSlot.contains(localTime)) {
            result = LocalDateTime.of(localDate, weeklyBookedTimeSlot.getEnd());
        }
        return normalize(result, timeFrame);
    }

    @Override
    public LocalDateTime findTimeSlotEnd(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalDateTime result = timeFrame.getEnd();
        LocalTime localTime = beginning.toLocalTime();
        LocalDate localDate = beginning.toLocalDate();
        Map.Entry<LocalTime, LocalTimeSlot> nextBookedEntry = weeklyBooked.get(localDate.getDayOfWeek()).ceilingEntry(localTime);
        result = nextBookedEntry == null ? result : LocalDateTime.of(localDate, nextBookedEntry.getValue().getBeginning());
        return normalize(result, timeFrame);
    }
}
