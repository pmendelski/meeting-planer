package com.meetings.model.booking;

import com.meetings.model.TemporalSlot;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.LocalTimeSlot;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Filters every day booked time slots.
 */
public class DailyBookingFilter extends BookingFilter {

    private TreeMap<LocalTime, LocalTimeSlot> dailyBooked;

    public DailyBookingFilter(List<LocalTimeSlot> dailyBookings) {
        this.dailyBooked = TemporalSlot.mapByBeginning(dailyBookings);
    }

    protected LocalDateTime findTimeSlotBeginning(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalDateTime result = beginning;
        LocalTime fromTime = beginning.toLocalTime();
        LocalDate fromDate = beginning.toLocalDate();
        Map.Entry<LocalTime, LocalTimeSlot> bookedDailyBeforeEntry = dailyBooked.floorEntry(fromTime);
        if (bookedDailyBeforeEntry != null) {
            LocalTimeSlot bookedTimeSlot = bookedDailyBeforeEntry.getValue();
            if (bookedTimeSlot != null && bookedTimeSlot.contains(fromTime)) {
                result = LocalDateTime.of(fromDate, bookedTimeSlot.getEnd());
            }
        }
        return normalize(result, timeFrame);
    }

    protected LocalDateTime findTimeSlotEnd(LocalDateTimeSlot timeFrame) {
        LocalDateTime result = timeFrame.getEnd();
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalTime beginningTime = beginning.toLocalTime();
        LocalDate beginningDate = beginning.toLocalDate();
        Map.Entry<LocalTime, LocalTimeSlot> nextBookedEntry = dailyBooked.ceilingEntry(beginningTime);
        result = nextBookedEntry == null ? result : LocalDateTime.of(beginningDate, nextBookedEntry.getValue().getBeginning());
        return normalize(result, timeFrame);
    }
}
