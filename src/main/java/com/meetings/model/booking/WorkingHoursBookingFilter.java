package com.meetings.model.booking;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.LocalTimeSlot;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Filters working hours.
 */
public class WorkingHoursBookingFilter extends BookingFilter {

    private LocalTimeSlot workingHours;

    public WorkingHoursBookingFilter(LocalTimeSlot workingHours) {
        this.workingHours = workingHours;
    }

    protected LocalDateTime findTimeSlotBeginning(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalTime fromTime = beginning.toLocalTime();
        LocalDate fromDate = beginning.toLocalDate();
        if (workingHours.isAfter(fromTime)) {
            return min(LocalDateTime.of(fromDate, workingHours.getBeginning()), timeFrame.getEnd());
        }
        if (workingHours.isBefore(fromTime)) {
            return min(LocalDateTime.of(fromDate.plusDays(1), workingHours.getBeginning()), timeFrame.getEnd());
        }
        return beginning;
    }

    protected LocalDateTime findTimeSlotEnd(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalDateTime end = timeFrame.getEnd();
        if (workingHours.contains(beginning.toLocalTime())) {
            if (beginning.toLocalDate().equals(end.toLocalDate()) && workingHours.contains(end.toLocalTime())) {
                return LocalDateTime.of(beginning.toLocalDate(), end.toLocalTime());
            } else {
                return LocalDateTime.of(beginning.toLocalDate(), workingHours.getEnd());
            }
        }
        return end;
    }

    private LocalDateTime min(LocalDateTime d1, LocalDateTime d2) {
        return d1.isBefore(d2) ? d1 : d2;
    }
}
