package com.meetings.model.booking;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Filters weekends.
 */
public class WeekendBookingFilter extends BookingFilter {

    protected LocalDateTime findTimeSlotBeginning(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalDateTime result = beginning;
        LocalDate localDate = beginning.toLocalDate();
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        int i = 0;
        switch (dayOfWeek) {
            case SATURDAY:
                i = 2;
                break;
            case SUNDAY:
                i = 1;
                break;
        }
        result = i == 0 ? result : LocalDateTime.of(localDate.plusDays(i), LocalTime.MIN);
        return normalize(result, timeFrame);
    }

    protected LocalDateTime findTimeSlotEnd(LocalDateTimeSlot timeFrame) {
        LocalDateTime beginning = timeFrame.getBeginning();
        LocalDate localDate = beginning.toLocalDate();
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        int i = 0;
        switch (dayOfWeek) {
            case MONDAY:
                i = 5;
                break;
            case TUESDAY:
                i = 4;
                break;
            case WEDNESDAY:
                i = 3;
                break;
            case THURSDAY:
                i = 2;
                break;
            case FRIDAY:
                i = 1;
                break;
        }
        LocalDateTime result = i == 0 ? beginning : LocalDateTime.of(localDate.plusDays(i), LocalTime.MIN);
        return normalize(result, timeFrame);
    }
}
