package com.meetings.model;

import java.util.Comparator;

/**
 * Sorts MeetingSlots by attendee count and time slot start time.
 */
public class MeetingSlotComparator implements Comparator<MeetingSlot> {

    @Override
    public int compare(MeetingSlot slotA, MeetingSlot slotB) {
        int attendeesCmp = compareAttendees(slotA, slotB);
        return attendeesCmp != 0 ? attendeesCmp : compareStartTime(slotA, slotB);
    }

    private int compareStartTime(MeetingSlot slotA, MeetingSlot slotB) {
        return slotA.getTimeSlot().getBeginning().compareTo(slotB.getTimeSlot().getBeginning());
    }

    private int compareAttendees(MeetingSlot slotA, MeetingSlot slotB) {
        Integer slotASize = slotA.getIncludedAttendees().size();
        Integer slotBSize = slotB.getIncludedAttendees().size();
        return -1 * slotASize.compareTo(slotBSize);
    }

}
