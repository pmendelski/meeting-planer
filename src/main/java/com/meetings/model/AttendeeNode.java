package com.meetings.model;

import com.meetings.model.TemporalSlot.LocalDateTimeSlot;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Represents Attendee node.
 * It is optimized for MeetingPlaner.
 */
public class AttendeeNode {

    private Attendee attendee;

    private TreeMap<LocalDateTime, LocalDateTimeSlot> timeSlotMap;

    private List<LocalDateTimeSlot> timeSlots;

    public AttendeeNode(Attendee attendee, List<LocalDateTimeSlot> timeSlots) {
        this.attendee = attendee;
        this.timeSlotMap = TemporalSlot.mapByBeginning(timeSlots);
        this.timeSlots = timeSlots;
    }

    public Attendee getAttendee() {
        return attendee;
    }

    public List<LocalDateTimeSlot> getTimeSlots() {
        return timeSlots;
    }

    public LocalDateTimeSlot findTimeSlot(LocalDateTime dateTime) {
        Entry<LocalDateTime, LocalDateTimeSlot> entry = timeSlotMap.floorEntry(dateTime);
        return entry == null ? null : entry.getValue();
    }

}
