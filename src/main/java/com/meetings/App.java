package com.meetings;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.meetings.AppArguments.Defaults;
import com.meetings.model.MeetingPlanner;
import com.meetings.model.Attendee;
import com.meetings.model.AttendeeBuilder;
import com.meetings.model.MeetingSlot;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.parser.AttendeeDto;
import com.meetings.parser.Parser;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Application entry point.
 */
public class App implements AppArguments.ActionManager {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    private PrintStream out;

    public static void main(String[] args) throws Exception {
        main(AppArguments.build(args));
    }

    public static void main(AppArguments args) throws Exception {
        args.dispatch(new App(System.out));
    }

    private static String getStringFromInputStream(InputStream is) {
        return new Scanner(is).useDelimiter("\\A").next();
    }

    public App(PrintStream out) {
        this.out = out;
    }

    @Override
    public List<MeetingSlot> execute(Path input, LocalDateTimeSlot timeFrame, ZoneOffset offset, Duration duration) throws Exception {
        printProgramArguments(input, timeFrame, offset, duration);
        Parser parser = new Parser();
        AttendeeBuilder attendeeConverter = new AttendeeBuilder();
        MeetingPlanner meetingPlanner = new MeetingPlanner(offset);

        List<AttendeeDto> attendeeDtos = parser.parse(input);
        List<Attendee> attendees = attendeeConverter.build(attendeeDtos);
        List<MeetingSlot> meetingSlots = meetingPlanner.findBestMeetingSlots(attendees, timeFrame, duration);
        logger.info("Result {}", meetingSlots);
        printResult(meetingSlots);
        return meetingSlots;
    }

    @Override
    public void printInputHelp() throws Exception {
        String help = getStringFromInputStream(this.getClass().getClassLoader().getResourceAsStream("help/input.txt"));
        out.println(help);
    }

    @Override
    public void printSampleInput() throws Exception {
        String sample = getStringFromInputStream(this.getClass().getClassLoader().getResourceAsStream("sample.json"));
        out.println(sample);
    }

    @Override
    public void printUnrecognizedArguments(List<String> arguments) throws Exception {
        out.println("Found unrecognized program arguments: " + arguments);
        out.println("Please read the manual. Use --help argument.");
    }

    @Override
    public void printMissingArguments(List<String> arguments) throws Exception {
        out.println("Missing required arguments: " + arguments);
        out.println("Please read the manual. Use --help argument.");
    }

    @Override
    public void setLogLevel(String logLevel) throws Exception {
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.valueOf(logLevel));
    }

    @Override
    public void printHelp() throws Exception {
        Properties projectProperties = readProjectProperties();
        OffsetDateTime now = OffsetDateTime.now();
        DateTimeFormatter dateFormatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .append(DateTimeFormatter.ISO_LOCAL_DATE)
                .appendLiteral('T')
                .appendPattern("hh:mm")
                .toFormatter();
        String help = getStringFromInputStream(this.getClass().getClassLoader().getResourceAsStream("help/manual.txt"));
        for (String key : projectProperties.stringPropertyNames()) {
            help = help.replaceAll(Pattern.quote("{" + key + "}"), projectProperties.getProperty(key));
        }
        help = help.replaceAll(Pattern.quote("{defaultOffset}"), Defaults.OFFSET.toString());
        help = help.replaceAll(Pattern.quote("{defaultBeginning}"), dateFormatter.format(Defaults.BEGINNING));
        help = help.replaceAll(Pattern.quote("{defaultEnd}"), dateFormatter.format(Defaults.END));
        help = help.replaceAll(Pattern.quote("{defaultDuration}"), Long.toString(Defaults.DURATION.getSeconds() / 60));
        out.println(help);
    }

    private void printProgramArguments(Path input, LocalDateTimeSlot timeFrame, ZoneOffset offset, Duration duration) {
        out.println();
        out.println("Arguments:");
        out.println("===============================================================");
        out.println("timeFrame: " + timeFrame.getBeginning() + " " + timeFrame.getEnd());
        out.println("resultZoneOffset: " + offset);
        out.println("duration: " + duration.getSeconds() / 60 + " minutes");
        out.println("input: " + input);
        out.println();
    }

    private void printResult(List<MeetingSlot> meetingSlots) {
        out.println();
        out.println("Results:");
        out.println("===============================================================");
        if (meetingSlots.size() == 0) {
            out.println(" > Problem is unsolvable.");
        } else {
            MeetingSlot bestMeetingSlot = meetingSlots.get(0);
            if (bestMeetingSlot.getExcludedAttendees().size() == 0) {
                out.println(" > Found at least one meeting slot for all attendees");
            } else {
                out.println(" > Could not find meeting slot for all attendees");
            }
            out.println();
            printMeetingSlots(meetingSlots);
        }
    }

    private void printMeetingSlots(List<MeetingSlot> meetingSlots) {
        out.printf("%16s     %16s     %16s", "Beginning", "End", "Duration[min]");
        out.println();
        meetingSlots.stream()
                .forEach(meetingSlot -> {
                    LocalDateTimeSlot dateTimeSlot = meetingSlot.getTimeSlot();
                    String included = meetingSlot.getIncludedAttendees().stream().map(Attendee::getId)
                            .collect(Collectors.joining(", "));
                    String excluded = meetingSlot.getExcludedAttendees().stream().map(Attendee::getId)
                            .collect(Collectors.joining(", "));
                    out.printf("%16s     %16s     %16s", dateTimeSlot.getBeginning(), dateTimeSlot.getEnd(), dateTimeSlot.getDuration().getSeconds() / 60);
                    if (excluded.length() > 0) {
                        out.printf("  Included: %s", included);
                        out.printf("  Excluded: %s", excluded);
                    }
                    out.println();
                });
    }

    private Properties readProjectProperties() throws IOException {
        Properties properties = new Properties();
        try (InputStream input = App.class.getClassLoader().getResourceAsStream("help/project.properties")) {
            properties.load(input);
        }
        return properties;
    }

}
