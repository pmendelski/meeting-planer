package com.meetings;

import com.meetings.model.MeetingSlot;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Parses application runtime arguments.
 */
public class AppArguments {

    public static class Defaults {
        public static Duration DURATION = Duration.ofMinutes(45);
        public static ZoneOffset OFFSET = OffsetDateTime.now().getOffset();
        public static LocalDateTime BEGINNING = LocalDateTime.now();
        public static LocalDateTime END = LocalDateTime.now().plusWeeks(1);
    }

    private Path input;
    private Duration duration = Defaults.DURATION;
    private ZoneOffset zoneOffset = Defaults.OFFSET;
    private LocalDateTime beginning = Defaults.BEGINNING;
    private LocalDateTime end = Defaults.END;
    private boolean sample;
    private boolean help;
    private boolean inputHelp;
    private String logLevel;
    private List<String> unrecognized = new ArrayList<>();

    static AppArguments build(String... args) {
        AppArguments arguments = new AppArguments();
        for (int i = 0; i < args.length; ++i) {
            String arg = args[i];
            switch (arg) {
                case "--from":
                case "-f":
                    arguments.beginning = parseDateTime(args[++i]);
                    break;
                case "--to":
                case "-t":
                    arguments.end = parseDateTime(args[++i]);
                    break;
                case "--duration":
                case "-d":
                    arguments.duration = Duration.ofMinutes(Long.parseLong(args[++i]));
                    break;
                case "--offset":
                case "-o":
                    arguments.zoneOffset = ZoneOffset.of(args[++i]);
                    break;
                case "--log":
                case "-l":
                    arguments.logLevel = args[++i];
                    break;
                case "--help":
                case "-h":
                    arguments.help = true;
                    break;
                case "--sample":
                case "-s":
                    arguments.sample = true;
                    break;
                case "--input-help":
                case "-ih":
                    arguments.inputHelp = true;
                    break;
                default:
                    if (args.length < 1 || i < args.length - 1 || arg.startsWith("-")) {
                        arguments.unrecognized.add(arg);
                    } else {
                        arguments.input = Paths.get(arg);
                    }
            }
        }
        return arguments;
    }

    private static LocalDateTime parseDateTime(String input) {
        return LocalDateTime.parse(input, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    /**
     * Based on program arguments dispatches program action.
     *
     * @param manager
     * @throws Exception
     */
    public void dispatch(ActionManager manager) throws Exception {
        if (help) {
            manager.printHelp();
        } else if (inputHelp) {
            manager.printInputHelp();
        } else if (sample) {
            manager.printSampleInput();
        } else if (unrecognized.size() > 0) {
            manager.printUnrecognizedArguments(unrecognized);
        } else if (input != null) {
            if (logLevel != null) {
                manager.setLogLevel(logLevel);
            }
            manager.execute(input, new LocalDateTimeSlot(beginning, end), zoneOffset, duration);
        } else {
            manager.printMissingArguments(Arrays.asList("INPUT"));
        }
    }

    @Override
    public String toString() {
        return "AppArguments{" +
                "input=" + input +
                ", duration=" + duration +
                ", zoneOffset=" + zoneOffset +
                ", beginning=" + beginning +
                ", end=" + end +
                ", help=" + help +
                ", inputHelp=" + inputHelp +
                ", logLevel='" + logLevel + '\'' +
                ", unrecognized=" + unrecognized +
                '}';
    }

    /**
     * Executes application actions.
     */
    public interface ActionManager {

        /**
         * Prints program manual.
         *
         * @throws Exception
         */
        void printHelp() throws Exception;

        /**
         * Prints minimal sample input.
         *
         * @throws Exception
         */
        void printInputHelp() throws Exception;

        /**
         * Prints sample input.
         *
         * @throws Exception
         */
        void printSampleInput() throws Exception;

        /**
         * Prints unrecognized program arguments.
         *
         * @param arguments
         * @throws Exception
         */
        void printUnrecognizedArguments(List<String> arguments) throws Exception;

        /**
         * Prints missing required program arguments
         *
         * @param arguments
         * @throws Exception
         */
        void printMissingArguments(List<String> arguments) throws Exception;

        /**
         * Changes program logging level.
         *
         * @param logLevel
         * @throws Exception
         */
        void setLogLevel(String logLevel) throws Exception;

        /**
         * Executes main action. Finds available meeting slots.
         *
         * @param input     - JSON file with list of attendees
         * @param timeFrame - time frame constraint
         * @param offset    - offset of the time frame and the result
         * @param duration  - meeting duration
         * @return
         * @throws Exception
         */
        List<MeetingSlot> execute(Path input, LocalDateTimeSlot timeFrame, ZoneOffset offset, Duration duration) throws Exception;
    }
}
