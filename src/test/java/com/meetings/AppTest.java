package com.meetings;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.meetings.model.MeetingSlot;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Unit test for simple App.
 */
public class AppTest {

    private App app = new App(Mockito.mock(PrintStream.class));

    @Test
    public void shouldPrintHelpWithoutError() throws Exception {
        app.printHelp();
    }

    @Test
    public void shouldPrintInputHelpWithoutError() throws Exception {
        app.printInputHelp();
    }

    @Test
    public void shouldPrintMissingArgumentsWithoutError() throws Exception {
        app.printMissingArguments(Arrays.asList("param"));
    }

    @Test
    public void shouldPrintUnrecognizedArguments() throws Exception {
        app.printUnrecognizedArguments(Arrays.asList("param"));
    }

    @Test
    public void shouldSetLogLevel() throws Exception {
        // given
        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        Level backupLevel = root.getLevel();

        // when
        app.setLogLevel(Level.DEBUG.toString());

        // then
        Level level = root.getLevel();
        root.setLevel(backupLevel);
        Assertions.assertThat(level).isEqualTo(Level.DEBUG);
    }

    @Test
    public void shouldSolveSimpleProblemInstance() throws Exception {
        // given
        Path input = Paths.get(this.getClass().getResource("simple.json").toURI());
        Duration duration = Duration.ofMinutes(45);
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T00:00", "2015-06-30T00:00");
        ZoneOffset offset = ZoneOffset.of("+00:00");

        // when
        List<MeetingSlot> meetingSlots = app.execute(input, timeFrame, offset, duration);

        // then
        List<LocalDateTimeSlot> dateTimeSlots = meetingSlots.stream()
                .map(MeetingSlot::getTimeSlot)
                .collect(Collectors.toList());
        Assertions.assertThat(dateTimeSlots).containsSequence(
                Utils.createDateTimeSlot("2015-06-29T09:00", "2015-06-29T11:00"),
                Utils.createDateTimeSlot("2015-06-29T14:00", "2015-06-29T15:00")
        );
    }

    @Test
    public void shouldShowPartialResults() throws Exception {
        // given
        Path input = Paths.get(this.getClass().getResource("simple.json").toURI());
        Duration duration = Duration.ofMinutes(45);
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T00:00", "2015-06-29T09:00");
        ZoneOffset offset = ZoneOffset.of("+00:00");

        // when
        List<MeetingSlot> meetingSlots = app.execute(input, timeFrame, offset, duration);

        // then
        List<LocalDateTimeSlot> dateTimeSlots = meetingSlots.stream()
                .map(MeetingSlot::getTimeSlot)
                .collect(Collectors.toList());
        Assertions.assertThat(dateTimeSlots).containsSequence(
                Utils.createDateTimeSlot("2015-06-29T07:00", "2015-06-29T09:00")
        );
    }

    @Test
    public void shouldReturnEmptySolution() throws Exception {
        // given
        Path input = Paths.get(this.getClass().getResource("simple.json").toURI());
        Duration duration = Duration.ofMinutes(140);
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T14:00", "2015-06-30T00:00");
        ZoneOffset offset = ZoneOffset.of("+00:00");

        // when
        List<MeetingSlot> meetingSlots = app.execute(input, timeFrame, offset, duration);

        // then
        Assertions.assertThat(meetingSlots).isEmpty();
    }
}
