package com.meetings;

import com.meetings.BasicActionManager.Action;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class AppArgumentsTest {

    @Test
    public void shouldExecuteHelpAction() throws Exception {
        shouldExecuteAction("--help", "printHelp");
    }

    @Test
    public void shouldExecuteHelpActionOnAbbreviation() throws Exception {
        shouldExecuteAction("-h", "printHelp");
    }

    @Test
    public void shouldExecuteInputHelpAction() throws Exception {
        shouldExecuteAction("--input-help", "printInputHelp");
    }

    @Test
    public void shouldExecutePrintSampleAction() throws Exception {
        shouldExecuteAction("--sample", "printSampleInput");
    }

    @Test
    public void shouldExecuteInputHelpActionOnAbbreviation() throws Exception {
        shouldExecuteAction("-ih", "printInputHelp");
    }

    @Test
    public void shouldPrintMissingArguments() throws Exception {
        // given
        BasicActionManager actionManager = new BasicActionManager();
        AppArguments appArgs = AppArguments.build();

        // when
        appArgs.dispatch(actionManager);

        // then
        List<Action> actions = actionManager.getExecutedActions();
        Assertions.assertThat(actions.size()).isEqualTo(1);
        Assertions.assertThat(actions.get(0).getName()).isEqualTo("printMissingArguments");
        Assertions.assertThat(actions.get(0).getArgs()).contains(Arrays.asList("INPUT"));
    }

    @Test
    public void shouldExecuteTheAppWithDefaultValues() throws Exception {
        // given
        BasicActionManager actionManager = new BasicActionManager();
        AppArguments appArgs = AppArguments.build("input.json");

        // when
        appArgs.dispatch(actionManager);

        // then
        List<Action> actions = actionManager.getExecutedActions();
        Assertions.assertThat(actions.size()).isEqualTo(1);
        Assertions.assertThat(actions.get(0).getName()).isEqualTo("execute");
        Assertions.assertThat(actions.get(0).getArgs()).contains(Paths.get("input.json"));
        Assertions.assertThat(actions.get(0).getArgs()).doesNotContain(new Object[]{null});
    }

    @Test
    public void shouldSetLogLevelBeforeExecutingTheApp() throws Exception {
        // given
        BasicActionManager actionManager = new BasicActionManager();
        AppArguments appArgs = AppArguments.build("-l", "TRACE", "input.json");

        // when
        appArgs.dispatch(actionManager);

        // then
        List<Action> actions = actionManager.getExecutedActions();
        Assertions.assertThat(actions.size()).isEqualTo(2);
        Assertions.assertThat(actions.get(0).getName()).isEqualTo("setLogLevel");
        Assertions.assertThat(actions.get(0).getArgs()).containsExactly("TRACE");
        Assertions.assertThat(actions.get(1).getName()).isEqualTo("execute");
    }

    private void shouldExecuteAction(String argument, String expectedAction, Object... expectedArgs) throws Exception {
        // given
        BasicActionManager actionManager = new BasicActionManager();
        AppArguments appArgs = AppArguments.build(argument);

        // when
        appArgs.dispatch(actionManager);

        // then
        List<Action> actions = actionManager.getExecutedActions();
        Assertions.assertThat(actions.size()).isEqualTo(1);
        Assertions.assertThat(actions.get(0).getName()).isEqualTo(expectedAction);
        Assertions.assertThat(actions.get(0).getArgs()).containsExactly(expectedArgs);
    }

}