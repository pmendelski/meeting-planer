package com.meetings.parser;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ParserTest {

    private Parser parser = new Parser();

    @Test
    public void shouldParseZeroAttendees() throws Exception {
        // given
        Path inputPath = Paths.get(this.getClass().getResource("zero-attendees.json").toURI());

        // when
        List<AttendeeDto> attendees = parser.parse(inputPath);

        // then
        Assertions.assertThat(attendees).isNotNull();
        Assertions.assertThat(attendees).isEmpty();
    }

    @Test
    public void shouldParseSingleAttendees() throws Exception {
        // given
        Path inputPath = Paths.get(this.getClass().getResource("single-attendee.json").toURI());

        // when
        List<AttendeeDto> attendees = parser.parse(inputPath);

        // then
        Assertions.assertThat(attendees).isNotNull();
        Assertions.assertThat(attendees.size()).isEqualTo(1);
    }

    @Test
    public void shouldParseTwoAttendees() throws Exception {
        // given
        Path inputPath = Paths.get(this.getClass().getResource("two-attendees.json").toURI());

        // when
        List<AttendeeDto> attendees = parser.parse(inputPath);

        // then
        Assertions.assertThat(attendees).isNotNull();
        Assertions.assertThat(attendees.size()).isEqualTo(2);
    }

}