package com.meetings.parser.deserializer;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.time.DayOfWeek;

public class DayOfWeekKeyDeserializerTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private DayOfWeekKeyDeserializer deserializer = new DayOfWeekKeyDeserializer();

    @Test
    public void shouldDeserializeLowercasedMonday() throws IOException {
        // given
        String input = "monday";

        // when
        Object result = deserializer.deserializeKey(input, null);

        // then
        Assertions.assertThat(result).isEqualTo(DayOfWeek.MONDAY);
    }

    @Test
    public void shouldDeserializeUppercasedMonday() throws IOException {
        // given
        String input = "MONDAY";

        // when
        Object result = deserializer.deserializeKey(input, null);

        // then
        Assertions.assertThat(result).isEqualTo(DayOfWeek.MONDAY);
    }

    @Test
    public void shouldThrowExceptionOnIncorrectDayOfWeek() throws IOException {
        // given
        String input = "mondayx";

        // then
        exception.expect(IllegalArgumentException.class);

        // when
        deserializer.deserializeKey(input, null);
    }

}