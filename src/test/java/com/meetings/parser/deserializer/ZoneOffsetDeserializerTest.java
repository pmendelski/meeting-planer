package com.meetings.parser.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.ZoneOffset;

/**
 * Created by pmendelski on 2015-06-29.
 */
public class ZoneOffsetDeserializerTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private ZoneOffsetDeserializer deserializer = new ZoneOffsetDeserializer();

    @Test
    public void shouldDeserializeOffset() throws IOException {
        // given
        String input = "+01:00";

        // when
        Object result = deserializer.deserialize(jsonParser(input), null);

        // then
        Assertions.assertThat(result).isEqualTo(ZoneOffset.of(input));
    }

    @Test
    public void shouldThrowExceptionOnIncorrectOffset() throws IOException {
        // given
        String input = "+0100:00";

        // then
        exception.expect(DateTimeException.class);

        // when
        deserializer.deserialize(jsonParser(input), null);
    }

    @Test
    public void shouldThrowExceptionOnLettersInOffset() throws IOException {
        // given
        String input = "+01:XX";

        // then
        exception.expect(DateTimeException.class);

        // when
        deserializer.deserialize(jsonParser(input), null);
    }

    private JsonParser jsonParser(String value) throws IOException {
        JsonParser mock = Mockito.mock(JsonParser.class);
        Mockito.stub(mock.getText()).toReturn(value);
        return mock;
    }
}