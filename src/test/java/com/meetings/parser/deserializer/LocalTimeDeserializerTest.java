package com.meetings.parser.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Created by pmendelski on 2015-06-29.
 */
public class LocalTimeDeserializerTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private LocalTimeDeserializer deserializer = new LocalTimeDeserializer();

    @Test
    public void shouldDeserializeLocalTime() throws IOException {
        // given
        String input = "10:00";

        // when
        Object result = deserializer.deserialize(jsonParser(input), null);

        // then
        Assertions.assertThat(result).isEqualTo(LocalTime.parse(input, DateTimeFormatter.ISO_TIME));
    }

    @Test
    public void shouldThrowExceptionOnIncorrectLocalTime() throws IOException {
        // given
        String input = "25:00";

        // then
        exception.expect(DateTimeParseException.class);

        // when
        deserializer.deserialize(jsonParser(input), null);
    }

    @Test
    public void shouldThrowExceptionOnLettersInLocalTime() throws IOException {
        // given
        String input = "10:XX";

        // then
        exception.expect(DateTimeParseException.class);

        // when
        deserializer.deserialize(jsonParser(input), null);
    }

    private JsonParser jsonParser(String value) throws IOException {
        JsonParser mock = Mockito.mock(JsonParser.class);
        Mockito.stub(mock.getText()).toReturn(value);
        return mock;
    }
}