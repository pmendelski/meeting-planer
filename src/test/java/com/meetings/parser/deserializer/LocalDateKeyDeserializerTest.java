package com.meetings.parser.deserializer;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


public class LocalDateKeyDeserializerTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private LocalDateKeyDeserializer deserializer = new LocalDateKeyDeserializer();

    @Test
    public void shouldDeserializeLocalDate() throws IOException {
        // given
        String input = "2015-06-29";

        // when
        Object result = deserializer.deserializeKey(input, null);

        // then
        Assertions.assertThat(result).isEqualTo(LocalDate.parse(input, DateTimeFormatter.ISO_DATE));
    }

    @Test
    public void shouldThrowExceptionOnIncorrectLocalDate() throws IOException {
        // given
        String input = "2015-06-33";

        // then
        exception.expect(DateTimeParseException.class);

        // when
        deserializer.deserializeKey(input, null);
    }

    @Test
    public void shouldThrowExceptionOnLettersInLocalDate() throws IOException {
        // given
        String input = "2015-06-XX";

        // then
        exception.expect(DateTimeParseException.class);

        // when
        deserializer.deserializeKey(input, null);
    }

}