package com.meetings;


import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.LocalTimeSlot;
import com.meetings.model.TemporalSlot.OffsetDateTimeSlot;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class Utils {

    public static LocalTimeSlot createTimeSlot(String[] param) {
        return createTimeSlot(param[0], param[1]);
    }

    public static LocalTimeSlot createTimeSlot(String beginningInput, String endInput) {
        LocalTime beginning = createLocalTime(beginningInput);
        LocalTime end = createLocalTime(endInput);
        return new LocalTimeSlot(beginning, end);
    }

    public static List<LocalTimeSlot> createTimeSlots(String[][] params) {
        return Arrays.asList(params).stream()
                .map(Utils::createTimeSlot)
                .collect(Collectors.toList());
    }

    public static LocalDateTimeSlot createDateTimeSlot(String beginningInput, String endInput) {
        LocalDateTime beginning = createLocalDateTime(beginningInput);
        LocalDateTime end = createLocalDateTime(endInput);
        return new LocalDateTimeSlot(beginning, end);
    }

    public static OffsetDateTimeSlot createOffsetDateTimeSlot(String beginningInput, String endInput) {
        OffsetDateTime beginning = createOffsetDateTime(beginningInput);
        OffsetDateTime end = createOffsetDateTime(endInput);
        return new OffsetDateTimeSlot(beginning, end);
    }

    public static LocalDateTimeSlot createDateTimeSlot(String[] param) {
        return createDateTimeSlot(param[0], param[1]);
    }

    public static LocalDateTime createLocalDateTime(String input) {
        return LocalDateTime.parse(input, DateTimeFormatter.ISO_DATE_TIME);
    }

    public static OffsetDateTime createOffsetDateTime(String input) {
        return OffsetDateTime.parse(input, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static LocalTime createLocalTime(String input) {
        return LocalTime.parse(input, DateTimeFormatter.ISO_TIME);
    }

    public static List<LocalDateTimeSlot> createDateTimeSlots(String[][] params) {
        return Arrays.asList(params).stream()
                .map(Utils::createDateTimeSlot)
                .collect(Collectors.toList());
    }
}
