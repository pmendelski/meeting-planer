package com.meetings;

import com.meetings.AppArguments.ActionManager;
import com.meetings.model.MeetingSlot;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;

import java.nio.file.Path;
import java.time.Duration;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BasicActionManager implements ActionManager {

    private List<Action> executedActions = new ArrayList<>();

    @Override
    public void printHelp() throws Exception {
        executedActions.add(new Action("printHelp"));
    }

    @Override
    public void printInputHelp() throws Exception {
        executedActions.add(new Action("printInputHelp"));
    }

    @Override
    public void printSampleInput() throws Exception {
        executedActions.add(new Action("printSampleInput"));
    }

    @Override
    public void printUnrecognizedArguments(List<String> arguments) throws Exception {
        executedActions.add(new Action("printUnrecognizedArguments", arguments));
    }

    @Override
    public void printMissingArguments(List<String> arguments) throws Exception {
        executedActions.add(new Action("printMissingArguments", arguments));
    }

    @Override
    public void setLogLevel(String logLevel) throws Exception {
        executedActions.add(new Action("setLogLevel", logLevel));
    }

    @Override
    public List<MeetingSlot> execute(Path input, LocalDateTimeSlot timeFrame, ZoneOffset offset, Duration duration) throws Exception {
        executedActions.add(new Action("execute", input, timeFrame, offset, duration));
        return Collections.emptyList();
    }

    public List<Action> getExecutedActions() {
        return executedActions;
    }

    public static class Action {
        private String name;
        private Object[] args;

        public Action(String name, Object... args) {
            this.name = name;
            this.args = args;
        }

        public String getName() {
            return name;
        }

        public Object[] getArgs() {
            return args;
        }
    }
}