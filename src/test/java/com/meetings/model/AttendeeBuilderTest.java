package com.meetings.model;

import com.meetings.Utils;
import com.meetings.parser.AttendeeDto;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.time.LocalTime;
import java.time.ZoneOffset;


public class AttendeeBuilderTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void shouldBuildAttendee() throws IOException {
        // given
        AttendeeBuilder builder = new AttendeeBuilder();
        AttendeeDto dto = sampleAttendeeDto();

        // when
        Attendee attendee = builder.build(dto);

        // then
        Assertions.assertThat(attendee).isNotNull();
        Assertions.assertThat(attendee.getId()).isEqualTo(dto.id);
        Assertions.assertThat(attendee.getTimezone()).isEqualTo(dto.timezone);
    }

    @Test
    public void shouldThrowExceptionOnDuplicatedIds() throws IOException {
        // given
        AttendeeBuilder builder = new AttendeeBuilder();
        AttendeeDto dtoA = new AttendeeDto();
        AttendeeDto dtoB = new AttendeeDto();

        // then
        exception.expect(IllegalArgumentException.class);

        // when
        builder.build(dtoA);
        builder.build(dtoB);
    }

    @Test
    public void shouldThrowExceptionOnEmptyWorkingHours() throws IOException {
        // given
        AttendeeBuilder builder = new AttendeeBuilder();
        AttendeeDto dto = new AttendeeDto();
        dto.workingHours = null;

        // then
        exception.expect(IllegalArgumentException.class);

        // when
        builder.build(dto);
    }

    @Test
    public void shouldThrowExceptionOnEmptyTimeOffset() throws IOException {
        // given
        AttendeeBuilder builder = new AttendeeBuilder();
        AttendeeDto dto = new AttendeeDto();
        dto.timezone = null;

        // then
        exception.expect(IllegalArgumentException.class);

        // when
        builder.build(dto);
    }

    private AttendeeDto sampleAttendeeDto() {
        AttendeeDto dto = new AttendeeDto();
        dto.id = "john.doe";
        dto.timezone = ZoneOffset.of("+01:00");
        dto.workingHours = new LocalTime[]{
                Utils.createLocalTime("08:00"),
                Utils.createLocalTime("16:00")
        };
        return dto;
    }
}