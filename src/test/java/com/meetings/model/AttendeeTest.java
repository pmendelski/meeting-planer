package com.meetings.model;

import com.meetings.Utils;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.LocalTimeSlot;
import com.meetings.model.booking.*;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.ZoneOffset;
import java.util.*;

public class AttendeeTest {

    private Attendee attendee;

    @Before
    public void setUp() {
        List<BookingFilter> bookingFilters = new ArrayList<>();
        attendee = new Attendee("john.doe", ZoneOffset.of("+01:00"), bookingFilters);

        bookingFilters.add(new WeekendBookingFilter());
        bookingFilters.add(new WorkingHoursBookingFilter(Utils.createTimeSlot("08:00", "16:00")));
        bookingFilters.add(new DailyBookingFilter(Arrays.asList(
                Utils.createTimeSlot("10:00", "10:30"),
                Utils.createTimeSlot("12:00", "13:00")
        )));
        Map<DayOfWeek, List<LocalTimeSlot>> weeklyBookings = new HashMap<>();
        weeklyBookings.put(DayOfWeek.FRIDAY, Arrays.asList(
                Utils.createTimeSlot("15:00", "16:30")
        ));
        bookingFilters.add(new WeeklyBookingFilter(weeklyBookings));
        bookingFilters.add(new SpecificBookingFilter(Arrays.asList(
                Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00")
        )));
    }

    @Test
    public void shouldTranslateTimeSlotBetweenZones() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-24T00:00", "2015-06-25T00:00");

        // when
        LocalDateTimeSlot timeSlot = attendee.findNearestTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isNotNull();
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-24T08:00", "2015-06-24T10:00"));
    }

    @Test
    public void shouldFindFirstDailyTimeSlotForHugeTimeFrame() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-24T00:00", "2025-06-25T00:00");

        // when
        LocalDateTimeSlot timeSlot = attendee.findNearestTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isNotNull();
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-24T08:00", "2015-06-24T10:00"));
    }

    @Test
    public void shouldFindFirstMondayDailyTimeSlot() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-21T00:00", "2015-06-24T00:00");

        // when
        LocalDateTimeSlot timeSlot = attendee.findNearestTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isNotNull();
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-22T08:00", "2015-06-22T10:00"));
    }

    @Test
    public void shouldFindTimeSlotBetweenDailyBookings() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-22T10:00", "2015-06-22T12:30");

        // when
        LocalDateTimeSlot timeSlot = attendee.findNearestTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isNotNull();
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-22T10:30", "2015-06-22T12:00"));
    }

    @Test
    public void shouldFindTimeSlotBeforeFridayBooking() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-26T12:00", "2015-06-26T17:00");

        // when
        LocalDateTimeSlot timeSlot = attendee.findNearestTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isNotNull();
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-26T13:00", "2015-06-26T15:00"));
    }

    @Test
    public void shouldRespectSpecificBookingFilter() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T12:00", "2015-06-29T18:00");

        // when
        LocalDateTimeSlot timeSlot = attendee.findNearestTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isNotNull();
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T13:00", "2015-06-29T16:00"));
    }

    @Test
    public void shouldFindMorningTimeSlotsFromFourDays() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T00:00", "2015-07-05T18:00");

        // when
        List<LocalDateTimeSlot> timeSlots = attendee.findTimeSlots(timeFrame);

        // then
        Set<LocalDateTimeSlot> timeSlotSet = new HashSet<>(timeSlots);
        Assertions.assertThat(timeSlotSet).contains(
                Utils.createDateTimeSlot("2015-06-29T08:00", "2015-06-29T10:00"),
                Utils.createDateTimeSlot("2015-06-30T08:00", "2015-06-30T10:00"),
                Utils.createDateTimeSlot("2015-07-01T08:00", "2015-07-01T10:00"),
                Utils.createDateTimeSlot("2015-07-02T08:00", "2015-07-02T10:00"),
                Utils.createDateTimeSlot("2015-07-03T08:00", "2015-07-03T10:00")
        );
    }

}