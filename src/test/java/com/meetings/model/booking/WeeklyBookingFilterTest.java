package com.meetings.model.booking;

import com.meetings.Utils;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import com.meetings.model.TemporalSlot.LocalTimeSlot;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.DayOfWeek;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mendlik on 29.06.15.
 */
public class WeeklyBookingFilterTest {

    @Test
    public void shouldReturnTimeSlotFromTimeFrameStartToBooking() {
        // given
        WeeklyBookingFilter booking = createBooking(DayOfWeek.MONDAY, new String[]{"10:00", "11:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T09:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T09:00", "2015-06-29T10:00"));
    }

    @Test
    public void shouldReturnTimeSlotFromBookingEndToTimeFrameEnd() {
        // given
        WeeklyBookingFilter booking = createBooking(DayOfWeek.MONDAY, new String[]{"10:00", "12:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T14:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T12:00", "2015-06-29T14:00"));
    }

    @Test
    public void shouldReturnTimeSlotBetweenTwoBookings() {
        // given
        WeeklyBookingFilter booking = createBooking(DayOfWeek.MONDAY,
                new String[]{"10:00", "11:00"},
                new String[]{"12:00", "14:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T11:00", "2015-06-29T12:00"));
    }

    @Test
    public void shouldReturnWholeTimeFrame() {
        // given
        WeeklyBookingFilter booking = new WeeklyBookingFilter(Collections.emptyMap());
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(timeFrame);
    }

    @Test
    public void shouldReturnEmptyTimeSlot() {
        // given
        WeeklyBookingFilter booking = createBooking(DayOfWeek.MONDAY, new String[]{"10:00", "13:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot.getBeginning()).isEqualTo(Utils.createLocalDateTime("2015-06-29T13:00"));
        Assertions.assertThat(timeSlot.getDuration().isZero()).isTrue();
    }


    private WeeklyBookingFilter createBooking(DayOfWeek dayOfWeek, String[]... inputTimeSlots) {
        Map<DayOfWeek, List<LocalTimeSlot>> bookings = new HashMap<>();
        bookings.put(dayOfWeek, Utils.createTimeSlots(inputTimeSlots));
        return new WeeklyBookingFilter(bookings);
    }
}