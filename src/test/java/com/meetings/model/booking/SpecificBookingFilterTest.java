package com.meetings.model.booking;

import com.meetings.Utils;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Created by mendlik on 29.06.15.
 */
public class SpecificBookingFilterTest {

    @Test
    public void shouldReturnTimeSlotFromTimeFrameStartToBooking() {
        // given
        SpecificBookingFilter booking = createBooking(new String[]{"2015-06-29T10:00", "2015-06-29T11:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T09:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T09:00", "2015-06-29T10:00"));
    }

    @Test
    public void shouldReturnTimeSlotFromBookingEndToTimeFrameEnd() {
        // given
        SpecificBookingFilter booking = createBooking(new String[]{"2015-06-29T10:00", "2015-06-29T12:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T14:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T12:00", "2015-06-29T14:00"));
    }

    @Test
    public void shouldReturnTimeSlotBetweenTwoBookings() {
        // given
        SpecificBookingFilter booking = createBooking(
                new String[]{"2015-06-29T10:00", "2015-06-29T11:00"},
                new String[]{"2015-06-29T12:00", "2015-06-29T14:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T11:00", "2015-06-29T12:00"));
    }

    @Test
    public void shouldReturnWholeTimeFrame() {
        // given
        SpecificBookingFilter booking = createBooking(new String[]{"2015-06-28T10:00", "2015-06-28T13:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(timeFrame);
    }

    @Test
    public void shouldReturnEmptyTimeSlot() {
        // given
        SpecificBookingFilter booking = createBooking(new String[]{"2015-06-29T10:00", "2015-06-29T13:00"});
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot.getBeginning()).isEqualTo(Utils.createLocalDateTime("2015-06-29T13:00"));
        Assertions.assertThat(timeSlot.getDuration().isZero()).isTrue();
    }

    private SpecificBookingFilter createBooking(String[]... inputDateTimeSlots) {
        return new SpecificBookingFilter(Utils.createDateTimeSlots(inputDateTimeSlots));
    }
}
