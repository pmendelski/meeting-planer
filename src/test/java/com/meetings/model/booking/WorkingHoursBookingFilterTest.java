package com.meetings.model.booking;

import com.meetings.Utils;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class WorkingHoursBookingFilterTest {

    @Test
    public void shouldReturnTimeSlotFromTimeFrameStartToEndOfWorkingHours() {
        // given
        WorkingHoursBookingFilter booking = createBooking("08:00", "16:00");
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T09:00", "2015-06-29T16:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T09:00", "2015-06-29T16:00"));
    }

    @Test
    public void shouldReturnTimeSlotFromStartOfWorkingHoursToTimeFrameEnd() {
        // given
        WorkingHoursBookingFilter booking = createBooking("08:00", "16:00");
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T08:00", "2015-06-29T14:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T08:00", "2015-06-29T14:00"));
    }

    @Test
    public void shouldReturnTimeSlotFromEarlierDay() {
        // given
        WorkingHoursBookingFilter booking = createBooking("08:00", "16:00");
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-30T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T16:00"));
    }

    @Test
    public void shouldReturnWholeTimeFrame() {
        // given
        WorkingHoursBookingFilter booking = createBooking("08:00", "16:00");
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(timeFrame);
    }

    @Test
    public void shouldReturnEmptyTimeSlot() {
        // given
        WorkingHoursBookingFilter booking = createBooking("08:00", "16:00");
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T17:00", "2015-06-30T03:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot.getBeginning()).isEqualTo(Utils.createLocalDateTime("2015-06-30T03:00"));
        Assertions.assertThat(timeSlot.getDuration().isZero()).isTrue();
    }

    private WorkingHoursBookingFilter createBooking(String from, String to) {
        return new WorkingHoursBookingFilter(Utils.createTimeSlot(from, to));
    }
}