package com.meetings.model.booking;

import com.meetings.Utils;
import com.meetings.model.TemporalSlot.LocalDateTimeSlot;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Collections;

/**
 * Created by mendlik on 29.06.15.
 */
public class WeekendBookingFilterTest {

    private WeekendBookingFilter booking = new WeekendBookingFilter();

    @Test
    public void shouldReturnTimeSlotFromTimeFrameStartToSaturday() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-07-03T09:00", "2015-07-05T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-07-03T09:00", "2015-07-04T00:00"));
    }

    @Test
    public void shouldReturnTimeSlotFromMondayToTimeFrameEnd() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-07-04T10:00", "2015-07-07T14:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-07-06T00:00", "2015-07-07T14:00"));
    }

    @Test
    public void shouldReturnTimeSlotBetweenTwoWeekends() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-27T11:00", "2015-07-05T12:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(Utils.createDateTimeSlot("2015-06-29T00:00", "2015-07-04T00:00"));
    }

    @Test
    public void shouldReturnWholeTimeFrame() {
        // given
        WeeklyBookingFilter booking = new WeeklyBookingFilter(Collections.emptyMap());
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-29T10:00", "2015-06-29T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot).isEqualTo(timeFrame);
    }

    @Test
    public void shouldReturnEmptyTimeSlot() {
        // given
        LocalDateTimeSlot timeFrame = Utils.createDateTimeSlot("2015-06-27T10:00", "2015-06-28T13:00");

        // when
        LocalDateTimeSlot timeSlot = booking.ceilingTimeSlot(timeFrame);

        // then
        Assertions.assertThat(timeSlot.getBeginning()).isEqualTo(Utils.createLocalDateTime("2015-06-28T13:00"));
        Assertions.assertThat(timeSlot.getDuration().isZero()).isTrue();
    }
}