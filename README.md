# Meeting Planer

Plans meetings for all attendees across multiple time zones.

## Building project

To build project simply run:

```bash
mvn clean install
```

## Basic usage

- Print manual:

```bash
java -jar meetings.jar --help
```

- Generate sample input file:

```bash
java -jar meetings.jar --sample > ./attendees.json
```

- Find meeting time slots:

```bash
java -jar meetings.jar --from 2015-06-11T00:00 --to 2015-06-13T00:00 --offset +01:00 --duration 45 ./attendees.json
```
